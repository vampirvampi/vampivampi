<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201024033625 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE car (id INT AUTO_INCREMENT NOT NULL, mark VARCHAR(255) NOT NULL, year SMALLINT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE driver (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, num_vu INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE num_car (id INT AUTO_INCREMENT NOT NULL, driver_id INT DEFAULT NULL, car_id INT DEFAULT NULL, number VARCHAR(6) NOT NULL, region SMALLINT NOT NULL, INDEX IDX_67BAE733C3423909 (driver_id), UNIQUE INDEX UNIQ_67BAE733C3C6F69F (car_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE penalty (id INT AUTO_INCREMENT NOT NULL, num_car_id INT NOT NULL, amount INT NOT NULL, cause VARCHAR(255) DEFAULT NULL, INDEX IDX_AFE28FD87AE7A127 (num_car_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE num_car ADD CONSTRAINT FK_67BAE733C3423909 FOREIGN KEY (driver_id) REFERENCES driver (id)');
        $this->addSql('ALTER TABLE num_car ADD CONSTRAINT FK_67BAE733C3C6F69F FOREIGN KEY (car_id) REFERENCES car (id)');
        $this->addSql('ALTER TABLE penalty ADD CONSTRAINT FK_AFE28FD87AE7A127 FOREIGN KEY (num_car_id) REFERENCES num_car (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE num_car DROP FOREIGN KEY FK_67BAE733C3C6F69F');
        $this->addSql('ALTER TABLE num_car DROP FOREIGN KEY FK_67BAE733C3423909');
        $this->addSql('ALTER TABLE penalty DROP FOREIGN KEY FK_AFE28FD87AE7A127');
        $this->addSql('DROP TABLE car');
        $this->addSql('DROP TABLE driver');
        $this->addSql('DROP TABLE num_car');
        $this->addSql('DROP TABLE penalty');
    }
}
